import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from flask import Flask
from flask_restplus import Resource, Api
import json
from json import JSONEncoder
app = Flask(__name__)
from datetime import datetime
from yahooFinanceApi import *
import sys

@app.route("/ticker/<ticker>")
def home(ticker):
    fromDate = datetime(2020, 1, 1)
    toDate = datetime(2020, 1, 15)
    
    financeApi = YahooFinanceApi()
    stocksData = financeApi.getStockPricesInRange(ticker, fromDate, toDate)

    # TODO Move this to a service class.
    lowestPrice = sys.float_info.max
    lowestPriceIndex = -1
    highestPrice = sys.float_info.min
    highestPriceIndex = -1

    currentIndex = 0
    for stockData in stocksData:
        closingPrice = float(stockData.get("close"))
        if lowestPrice > closingPrice:
            lowestPrice = closingPrice
            lowestPriceIndex = currentIndex
        elif highestPrice < closingPrice:
            highestPrice = closingPrice
            highestPriceIndex = currentIndex
        currentIndex += 1

    # TODO Eventually "low" will be only applicable before an earnings report date, and "high" will be after.
    # A free earnings report date API is proving hard to find.
    stockHighLowData = {
        "low": {
            "date": datetime.fromtimestamp(stocksData[lowestPriceIndex]["date"]).strftime('%y/%m/%d'),
            "price": lowestPrice
        },
        "high": {
            "date": datetime.fromtimestamp(stocksData[highestPriceIndex]["date"]).strftime('%y/%m/%d'),
            "price": highestPrice
        }
    }

    return json.dumps(stockHighLowData)
