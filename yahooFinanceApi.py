from datetime import datetime
import requests
import time

class YahooFinanceApi:

    rapidApiHost = "apidojo-yahoo-finance-v1.p.rapidapi.com"
    rapidApiUrl = "https://" + rapidApiHost + "/stock/v2"

    def getEarningsDatesInRange(self, ticker: str, fromDate: datetime, toDate: datetime):
        url = self.rapidApiUrl + "/get-analysis"
        querystring = { "symbol": ticker }
        headers = {
            'x-rapidapi-host': self.rapidApiHost,
            'x-rapidapi-key': "4ad63811a9mshf0a7ae70a033547p16b0f8jsn344c13b6c46f"
        }

        return requests.request("GET", url, headers=headers, params=querystring)

    def getStockPricesInRange(self, ticker: str, fromDate: datetime, toDate: datetime):
        url = self.rapidApiUrl + "/get-historical-data"
        querystring = {
            "frequency": "1d",
            "filter": "history",
            "period1": str(int(fromDate.timestamp())),
            "period2": str(int(toDate.timestamp())),
            "symbol": ticker
        }
        headers = {
            'x-rapidapi-host': self.rapidApiHost,
            'x-rapidapi-key': "6051447277msh6c5d8d39dea6365p1449c1jsn613206566a14"    
        }

        historicalData = requests.request("GET", url, headers=headers, params=querystring)
        
        return historicalData.json().get('prices')
